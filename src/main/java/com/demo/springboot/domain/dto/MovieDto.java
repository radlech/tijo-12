package com.demo.springboot.domain.dto;

import java.io.Serializable;

public class MovieDto implements Serializable {

    private Integer movieId;
    private String title;
    private Integer year;
    private String image;

    public MovieDto() {}

    public MovieDto(int movieId, String title, int year, String image) {
        this.movieId = movieId;
        this.title = title;
        this.year = year;
        this.image = image;
    }

    public Integer getMovieId() {
        return movieId;
    }

    public String getTitle() {
        return title;
    }

    public Integer getYear() {
        return year;
    }

    public String getImage() {
        return image;
    }

    @Override
    public String toString() {
        return movieId + "; "+ title + "; " + year + "; " + image;
    }
}
