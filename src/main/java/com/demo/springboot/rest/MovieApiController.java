package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.domain.dto.MovieListDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MovieApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);

    final String CSV_FILE = "D:\\TiJO\\movies\\movies\\movies.csv";
    final String SPLIT_BY = ";";
    final String HEADER = "ID; TITLE; YEAR; IMAGE;";

    @RequestMapping(method = RequestMethod.GET, value = "/movies")
    @ResponseBody
    @CrossOrigin
    public MovieListDto getMovies() {

        LOGGER.info("--- get movies");

        List<MovieDto> movies = new ArrayList<>();

        BufferedReader br = null;
        String line = "";
        boolean isHeader = true;

        try {
            br = new BufferedReader(new FileReader(CSV_FILE));
            while ((line = br.readLine()) != null) {
                String[] lineArray = line.split(SPLIT_BY);
                if(isHeader == false)
                    movies.add(new MovieDto(Integer.parseInt(lineArray[0].trim()),lineArray[1].trim(),
                            Integer.parseInt(lineArray[2].trim()),lineArray[3].trim()));
                else
                    isHeader = false;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return new MovieListDto(movies);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/movies", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Void> createMovie(@RequestBody MovieDto createMovieDto) throws URISyntaxException {

        LOGGER.info("--- create movie");
        LOGGER.info("--- movieId: {}",createMovieDto.getMovieId());
        LOGGER.info("--- title: {}",createMovieDto.getTitle());
        LOGGER.info("--- year: {}",createMovieDto.getYear());
        LOGGER.info("--- image: {}",createMovieDto.getImage());

        MovieListDto movies = getMovies();
        List<MovieDto> moviesList = movies.getMovies();

        for(MovieDto movie : moviesList) {
            if ( movie.getMovieId() == createMovieDto.getMovieId() ) {
                return ResponseEntity.status(409).build();
            }
        }

        moviesList.add(new MovieDto(createMovieDto.getMovieId(),createMovieDto.getTitle(),
                createMovieDto.getYear(), createMovieDto.getImage()));

        try {
            PrintWriter writer = new PrintWriter(CSV_FILE);
            writer.println(HEADER);
            for(MovieDto movie : movies.getMovies()) {
                writer.println(movie);
            }
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return ResponseEntity.created(new URI("/movies/" + createMovieDto.getMovieId())).build();
    }
}
